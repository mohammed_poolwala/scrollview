function getFaceHeight(a, b, z) {
    var obj = {
        height: "auto",
        width: "auto",
        display: "block",
    };
    if (a > b) {
        if (b < 1024) {
            obj.width = b - 10 + "px";
        } else {
            obj.width = z - 10 + "px";
        }
    } else {
        obj.height = a + "px";
    }

    // return a > b ? (b < 1024 ? b : z) : a;
    return obj;
}
function desktopScript() {
    var windowH = $(window).outerHeight();
    var windowW = $(window).width();
    var bannerVideoColW = $(".col-grap").width();
    var bannerTextCol = $(".col-text").height();

    var faceVideo = $("#faceVideo");

    var earthH = $("#earth-img").height();
    var headerH = $("#header").height();
    // var truckBootm = $("#truck-end").height();

    faceVideo.css(getFaceHeight(windowH - headerH, bannerVideoColW, windowW));
    setTimeout(function () {
        faceVideo.get(0).play();
    }, 500);
    $(".col-text").css({
        "padding-bottom": (windowH - headerH - bannerTextCol) / 2 + "px",
        "padding-top": (windowH - headerH - bannerTextCol) / 2 + "px",
    });

    $("#earth-section").height((earthH * 40) / 100);

    var bannerH = $("#section-a-banner").height();
    var currentscroll = 0;
    var midSectionDuration = 4000; //windowH > 768 ? 4000 : 3000;
    var lastSectionDuration = 4000; //windowH > 768 ? 4000 : 3000;

    // Init Controller
    var controller = new ScrollMagic.Controller();

    /* Section Banner
======================================================
*/
    $("#face").fadeIn("slow");
    setTimeout(function () {
        $("#handCircle3").fadeIn("slow");
    }, 700);
    setTimeout(function () {
        $("#handCircle2").fadeIn("slow");
    }, 1100);

    /* Section 2
======================================================
*/
    // elements

    function displayText(clothWeightSlide, elem, plan) {
        clothWeightSlide.fromTo(
            elem,
            5,
            { opacity: 0, y: 500, display: "none" },
            {
                opacity: 1,
                y: 0,
                display: "block",
                onStart: function () {
                    if (plan && plan.length) {
                        for (let index = 0; index < plan.length; index++) {
                            const element = plan[index];
                            if (!$(element.id).hasClass("show-plan")) {
                                $(element.id).addClass("show-plan");
                                setTimeout(function () {
                                    $(element.id).removeClass("show-plan");
                                }, element.time);
                            }
                        }
                    }
                },
            }
        );

        clothWeightSlide.to(elem, 7, {
            opacity: 1,
            y: 0,
            display: "block",
        });
        clothWeightSlide.to(elem, 5, {
            opacity: 0,
            y: -500,
            display: "none",
        });
    }

    var sectionOne = bannerH + headerH;
    var clothWeightSlide = new TimelineMax();

    displayText(clothWeightSlide, "#sectionTwoText1");

    displayText(clothWeightSlide, "#sectionTwoText2", [
        {
            id: "#fly-plan",
            time: 6000,
        },
        {
            id: "#fly-plan2",
            time: 9000,
        },
        {
            id: "#fly-plan3",
            time: 14000,
        },
        {
            id: "#fly-plan4",
            time: 20000,
        },
        {
            id: "#fly-plan5",
            time: 20000,
        },
        {
            id: "#fly-plan6",
            time: 5000,
        },
        {
            id: "#fly-plan7",
            time: 5000,
        },
    ]);
    displayText(clothWeightSlide, "#sectionthreeText4", [
        {
            id: "#fly-plan",
            time: 6000,
        },
        {
            id: "#fly-plan2",
            time: 9000,
        },
        {
            id: "#fly-plan3",
            time: 14000,
        },
        {
            id: "#fly-plan4",
            time: 20000,
        },
        {
            id: "#fly-plan5",
            time: 20000,
        },
        {
            id: "#fly-plan7",
            time: 5000,
        },
    ]);
    displayText(clothWeightSlide, "#sectionthreeText3");
    // displayText(clothWeightSlide,  "#sectionthreeText2");
    clothWeightSlide.fromTo(
        "#clouds, .plane img",
        2,
        { opacity: 1 },
        { opacity: 0 },
        70
    );

    clothWeightSlide.add(
        TweenMax.fromTo(
            "#rain-cloth-end",
            5,
            { y: 500, display: "none" },
            { y: 0, display: "block" },
            70
        ),
        "first"
    );

    var trackText = new TimelineMax();
    trackText.fromTo(
        "#sectionthreeText2 h4",
        5,
        { opacity: 0 },
        { opacity: 1 }
    );
    trackText.fromTo(
        "#sectionthreeText2 h3",
        5,
        { opacity: 0 },
        { opacity: 1 }
    );
    trackText.fromTo("#sectionthreeText2 p", 5, { opacity: 0 }, { opacity: 1 });
    clothWeightSlide.add(trackText, "first");

    // Secound container pin #section-a-throw-cloth
    currentscroll = sectionOne;
    var firstSection = new ScrollMagic.Scene({
        offset: currentscroll,
        duration: midSectionDuration,
    })
        .setPin("#section-a-throw-cloth")
        .setTween(clothWeightSlide)
        .addTo(controller);

    /* Section 3
======================================================
*/

    function earthText(skyLightInst, elem, BG_sky, BG_earth, ss) {
        skyLightInst.add(
            TweenMax.fromTo(
                elem,
                ss === "first" ? 0 : 5,
                {
                    opacity: 0,
                    bottom: -100,
                },
                {
                    opacity: 1,
                    bottom: 0,
                    onStart: function () {
                        TweenMax.to("#earth-img", 1, { top: BG_earth });
                        TweenMax.to("#sky-img", 1, { top: BG_sky });
                    },
                }
            ),
            ss
        );
        skyLightInst.to(elem, 7, { bottom: 0 });
        if (ss !== "fiveth") {
            skyLightInst.to(elem, 5, { opacity: 0, bottom: 100 });
        }
    }

    function earthFinalText(skyLightInst, elem, BG_sky, BG_earth, ss) {
        var finalText = new TimelineMax();
        finalText.fromTo(
            "#sky-5 > p span",
            5,
            {
                opacity: 0,
                bottom: -100,
            },
            {
                opacity: 1,
                bottom: 0,
                onStart: function () {
                    TweenMax.to("#earth-img", 1, { top: BG_earth });
                    TweenMax.to("#sky-img", 1, { top: BG_sky });
                },
            }
        );
        finalText.fromTo(
            "#sky-5 h2 span",
            5,
            {
                opacity: 0,
                bottom: -100,
            },
            {
                opacity: 1,
                bottom: 0,
            }
        );
        finalText.fromTo(
            "#sky-5 .cta-section span",
            5,
            {
                opacity: 0,
                bottom: -100,
            },
            {
                opacity: 1,
                bottom: 0,
            }
        );

        skyLightInst.add(finalText, ss);
        skyLightInst.to(elem, 7, { bottom: 0 });
    }

    var sectionTwo = currentscroll + midSectionDuration + windowH;
    var skyLight = new TimelineMax();
    var secSection = new ScrollMagic.Scene({
        offset: sectionTwo,
        duration: lastSectionDuration,
    })
        .setPin("#section-a-sky")
        .addTo(controller);

    earthText(skyLight, "#sky-1 span", "-100%", "100%", "first");
    earthText(skyLight, "#sky-2 span", "-220%", "75%", "second");
    earthText(skyLight, "#sky-3 span", "-260%", "50%", "third");
    earthText(skyLight, "#sky-4 span", "-295%", "25%", "fourth");
    earthFinalText(skyLight, "#sky-5 span", "-330%", "0%", "fiveth");
    // skyLight.fromTo(
    //     "#sky-5",
    //     5,
    //     { opacity: 0, y: 500, display: "none" },
    //     { opacity: 1, y: -50, display: "block" }
    // );
    // skyLight.to("#sky-5", 7, { y: -50, display: "block" });
    new ScrollMagic.Scene({
        offset: sectionTwo,
        duration: lastSectionDuration,
    })
        .setTween(skyLight)
        .addTo(controller);

    var lastScrollTop = 0;
    var storeOuterH = 0;
    $(window).on("scroll", function () {
        var st = $(this).scrollTop();
        var winH = $(window).height();
        var winOuterH = $(window).outerHeight();
        if (st > lastScrollTop) {
            // downscroll code
            secSection.offset(currentscroll + midSectionDuration + winOuterH)
            storeOuterH = winOuterH;
            // $("#mp").html($(window).innerHeight() +  "  downscroll " + addH + " ___ " + winOuterH);
        } else {
            // upscroll code
            var addH = 0
            if( winH !== storeOuterH){
                addH = storeOuterH - winH
            } 
            secSection.offset(currentscroll + midSectionDuration + winH + addH)
            // $("#mp").html(addH +  "  upscroll" + winH + " ___ " + winOuterH );
        }
        lastScrollTop = st;
    });
}
