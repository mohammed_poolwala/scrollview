function mobileScript() {
    var currentscroll = 0;
    var headerH = $("#header").height();

    var bannerH = $("#section-a-banner").height();

    currentscroll = currentscroll + headerH + $("#section-a-banner .banner-wrapper .col-text").outerHeight()

    $(window).on("scroll", function () {
        var scroll = this.scrollY;
        console.log(scroll, $("#section-a-banner").height());
    });

    // Init Controller
    var controller = new ScrollMagic.Controller();

    /* Section Banner
======================================================
*/
    $("#face").fadeIn("slow");
    setTimeout(function () {
        $("#handCircle3").fadeIn("slow");
    }, 700);
    setTimeout(function () {
        $("#handCircle2").fadeIn("slow");
    }, 1100);

    // elements
    var faceTimeLine = new TimelineMax();

    faceTimeLine.fromTo(
        "#hand1",
        1,
        {
            bottom: "51%",
            left: "15%",
            rotation: -90,
            opacity: 0,
            display: "none",
        },
        {
            bottom: "51%",
            left: "15%",
            rotation: 0,
            opacity: 1,
            display: "block",
        }
    );
    faceTimeLine.fromTo("#hand2", 1, { y: "-100%" }, { y: "0%" });
    faceTimeLine.fromTo(
        "#hand3",
        1,
        {
            top: "0%",
            right: "41%",
            scale: 0.4,
            opacity: 0,
            display: "block",
        },
        { top: "0%", right: "41%", scale: 1, opacity: 1, display: "block" }
    );
    faceTimeLine.fromTo(
        "#faceFromCloth",
        1,
        { y: "-50%", x: "50%", scale: 0.2, opacity: 0 },
        { y: "0%", x: "0%", scale: 1, opacity: 1 }
    );

    // First container pin #section-a-banner
    var startpin = new ScrollMagic.Scene({
        offset: currentscroll,
        duration: 500,
    })
        .setPin("#section-a-banner .col-grap")
        .setTween(faceTimeLine)
        .addTo(controller);

    /* Section 2
======================================================
*/
    // elements
    var sectionOne = bannerH + headerH + 200;

    var clothRain = TweenMax.fromTo(
        "#rain-cloth-animation",
        1,
        { opacity: 1, y: "-100%" },
        { opacity: 1, y: "-40%" }
    );
    var clothRain2 = TweenMax.to("#rain-cloth-animation", 1, {
        opacity: 1,
        y: "10%",
    });

    var flyplan = TweenMax.fromTo(
        "#fly-plan",
        1,
        { opacity: 1, x: "-100%", y: "100%" },
        { opacity: 1, x: "0%", y: "0%" }
    );

    var sectionTwoText1 = TweenMax.fromTo(
        "#sectionTwoText1",
        1,
        { opacity: 0, y: 300, display: "none" },
        { opacity: 1, y: 50, display: "block" }
    );
    var sectionTwoText1Revers = TweenMax.to("#sectionTwoText1", 0.3, {
        opacity: 0,
        scale: 0,
        display: "none",
    });

    var sectionTwoText2 = TweenMax.fromTo(
        "#sectionTwoText2",
        1,
        { opacity: 0, y: 300, display: "none" },
        { opacity: 1, y: 50, display: "block" }
    );
    var sectionTwoText2Revers = TweenMax.to("#sectionTwoText2", 0.3, {
        opacity: 0,
        scale: 0,
        display: "none",
    });

    var tl = new TimelineMax();
    tl.to("#rain-cloth-wrapper", 1, {
        borderRadius: "50%",
        width: $("#section-a-throw-cloth").height(),
        height: $("#section-a-throw-cloth").height(),
    });
    tl.to("#rain-cloth-wrapper", 1, {
        opacity: 0,
        scale: 0,
        display: "none",
    });

    var clothWeightSlide = new TimelineMax();
    clothWeightSlide.fromTo(
        "#cloth-weight-wrwpper",
        0.3,
        { opacity: 0, scale: 0, display: "none" },
        { opacity: 1, scale: 1, display: "flex" }
    );
    clothWeightSlide.to("#buses", 3, {
        opacity: 1,
        scale: 1,
        display: "block",
    });
    clothWeightSlide.to("#buses", 7, {
        opacity: 1,
        scale: 1,
        display: "block",
    });

    clothWeightSlide.to("#buses", 0.1, {
        opacity: 0,
        scale: 0,
        display: "none",
    });
    clothWeightSlide.to("#jumbo-jet", 3, {
        opacity: 1,
        scale: 1,
        display: "block",
    });
    clothWeightSlide.to("#jumbo-jet", 7, {
        opacity: 1,
        scale: 1,
        display: "block",
    });

    clothWeightSlide.to("#jumbo-jet", 0.1, {
        opacity: 0,
        scale: 0,
        display: "none",
    });
    clothWeightSlide.to("#skyscraper", 3, {
        opacity: 1,
        scale: 1,
        display: "block",
    });
    clothWeightSlide.to("#skyscraper", 7, {
        opacity: 1,
        scale: 1,
        display: "block",
    });
    clothWeightSlide.to("#skyscraper", 0.1, {
        opacity: 0,
        scale: 0,
        display: "none",
    });

    clothWeightSlide.to("#cloth-weight-wrwpper", 0.1, {
        opacity: 0,
        scale: 0,
        display: "none",
    });
    clothWeightSlide.fromTo(
        "#rain-cloth-ground",
        0.1,
        { opacity: 1, y: "-100%" },
        { opacity: 1, y: "0%" }
    );

    clothWeightSlide.to("#rain-cloth-ground-wrapper", 5, {
        opacity: 1,
        scale: 1,
        display: "flex",
        borderRadius: "0%",
        width: "100%",
        height: $("#section-a-throw-cloth").height(),
    });

    clothWeightSlide.fromTo(
        "#sectionthreeText3",
        1,
        { opacity: 0, y: 300, display: "none" },
        { opacity: 1, y: 0, display: "block" }
    );
    clothWeightSlide.to("#sectionthreeText3", 7, {
        opacity: 1,
        y: 0,
        display: "block",
    });

    clothWeightSlide.to("#sectionthreeText3", 0.5, {
        opacity: 0,
        y: -300,
        display: "none",
    });
    clothWeightSlide.to("#rain-cloth-ground", 1, { opacity: 1, y: "-65%" });
    clothWeightSlide.fromTo(
        "#sectionthreeText2",
        1,
        { opacity: 0, y: 300, display: "none" },
        { opacity: 1, y: -100, display: "block" }
    );

    clothWeightSlide.fromTo(
        "#rain-cloth-end",
        1,
        { opacity: 0, y: 300, display: "none" },
        { opacity: 1, y: 50, display: "block" }
    );
    clothWeightSlide.to("#rain-cloth-end", 5, {
        y: 50,
    });
    clothWeightSlide.to("#rain-cloth-end", 1, {
        opacity: 1,
        y: 0,
        display: "block",
    });

    // Secound container pin #section-a-throw-cloth
    currentscroll = sectionOne + 300;
    var startpin = new ScrollMagic.Scene({
        offset: currentscroll,
        duration: 4000,
    })
        .setPin("#section-a-throw-cloth")
        .addTo(controller);

    //backGround Rain
    new ScrollMagic.Scene({
        offset: sectionOne + 200,
        duration: 300,
    })
        .setTween(clothRain)
        .addTo(controller);

    //sectionTwoText1
    new ScrollMagic.Scene({
        offset: sectionOne + 300,
        duration: 100,
    })
        .setTween(sectionTwoText1)
        .addTo(controller);

    new ScrollMagic.Scene({
        offset: sectionOne + 600,
        duration: 100,
    })
        .setTween(sectionTwoText1Revers)
        .addTo(controller);

    //backGround Rain 2
    new ScrollMagic.Scene({
        offset: sectionOne + 700,
        duration: 300,
    })
        .setTween(clothRain2)
        .addTo(controller);
    //sectionTwoText2
    new ScrollMagic.Scene({
        offset: sectionOne + 700,
        duration: 100,
    })
        .setTween(sectionTwoText2)
        .addTo(controller);

    new ScrollMagic.Scene({
        offset: sectionOne + 1400,
        duration: 100,
    })
        .setTween(sectionTwoText2Revers)
        .addTo(controller);

    //Fly Plan
    new ScrollMagic.Scene({
        offset: sectionOne + 700,
        duration: 300,
    })
        .setTween(flyplan)
        .addTo(controller);

    //Cloth
    new ScrollMagic.Scene({
        offset: sectionOne + 1000,
        duration: 200,
    })
        .setTween(tl)
        .addTo(controller);

    new ScrollMagic.Scene({
        offset: sectionOne + 1200,
        duration: 3000,
    })
        .setTween(clothWeightSlide)
        .addTo(controller);

    /* Section 3
======================================================
*/

    var sectionTwo =
        currentscroll + 1000 + 3000 + $("#section-a-throw-cloth").height();
    console.log("sectionTwo", sectionTwo);
    var skyLight = new TimelineMax();
    new ScrollMagic.Scene({
        offset: sectionTwo,
        duration: 4500,
    })
        .setPin("#section-a-sky")
        .addTo(controller);

    skyLight.fromTo(
        "#sky-1",
        5,
        { opacity: 0, y: 200, display: "none" },
        { opacity: 1, y: -50, display: "block" }
    );
    skyLight.to("#sky-1", 7, { y: -50, display: "block" });
    skyLight.to("#sky-1", 5, { opacity: 0, y: -200, display: "none" });

    skyLight.to("#sky-img", 15, { y: -250 });
    skyLight.fromTo(
        "#sky-2",
        5,
        { opacity: 0, y: 200, display: "none" },
        { opacity: 1, y: -50, display: "block" }
    );
    skyLight.to("#sky-2", 7, { y: -50, display: "block" });
    skyLight.to("#sky-2", 5, { opacity: 0, y: -200, display: "none" });

    skyLight.to("#sky-img", 15, { y: -350 });
    skyLight.fromTo(
        "#sky-3",
        5,
        { opacity: 0, y: 200, display: "none" },
        { opacity: 1, y: -50, display: "block" }
    );
    skyLight.to("#sky-3", 7, { y: -50, display: "block" });
    skyLight.to("#sky-3", 5, { opacity: 0, y: -200, display: "none" });

    skyLight.to("#sky-img", 15, { y: -450 });
    skyLight.fromTo(
        "#sky-4",
        5,
        { opacity: 0, y: 200, display: "none" },
        { opacity: 1, y: -50, display: "block" }
    );
    skyLight.to("#sky-4", 7, { y: -50, display: "block" });
    skyLight.to("#sky-4", 5, { opacity: 0, y: -200, display: "none" });

    skyLight.to("#sky-img", 15, { y: -700 });
    skyLight.fromTo(
        "#sky-5",
        5,
        { opacity: 0, y: 200, display: "none" },
        { opacity: 1, y: -50, display: "block" }
    );
    skyLight.to("#sky-5", 7, { y: -50, display: "block" });
    skyLight.to("#sky-5", 5, { opacity: 0, y: -200, display: "none" });

    skyLight.to("#sky-img", 15, { y: -750 });
    skyLight.fromTo(
        "#sky-6",
        5,
        { opacity: 0, y: 200, display: "none" },
        { opacity: 1, y: -50, display: "block" }
    );
    // skyLight.to("#sky-6", 0.3, { opacity: 0, y: -200, display: "none" });

    new ScrollMagic.Scene({
        offset: sectionTwo + 200,
        duration: 4000,
    })
        .setTween(skyLight)
        .addTo(controller);
}
