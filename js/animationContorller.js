$(function () {
    // desktopScript();
    document.body.style.overflow = "hidden";
    var imgs = document.images,
        len = imgs.length,
        counter = 0;

    [].forEach.call(imgs, function (img) {
        if (img.complete) incrementCounter();
        else img.addEventListener("load", incrementCounter, false);
    });

    function incrementCounter() {
        counter++;
        if (counter === len) {
            document.body.style.overflow = "auto";
            document.getElementById("page_loading").style.display = "none";
            desktopScript();
        }
    }

    return;
});
